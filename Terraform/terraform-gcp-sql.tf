resource "google_sql_database_instance" "master" {
  name             = "catenasql"
  database_version = "MYSQL_5_7"
  region           = "europe-west3"

  settings {
    # Second-generation instance tiers are based on the machine
    # type. See argument reference below.
    tier = "db-n1-standard-1"
  }
}