resource "google_sql_user" "users" {
  name     = "wp-db-user"
  instance = google_sql_database_instance.master.name
  host     = "%"
  password = ""
}

resource "google_sql_database" "database" {
  name     = "catena-test"
  instance = google_sql_database_instance.master.name
}