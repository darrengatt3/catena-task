resource "google_storage_bucket" "static-site" {
  name          = "catena-storage-website"
  location      = "europe-west3"
  force_destroy = true
  bucket_policy_only = true
}