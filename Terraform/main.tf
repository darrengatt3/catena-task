provider "google" {
  credentials = "${file("terraform-SA.json")}"
  project = "project-test-280815"
  region  = "europe-west1"
}